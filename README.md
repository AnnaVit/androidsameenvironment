# README #

Questo repository rappresenta il programma da poter installare ed eseguire su un dispositivo Android, il quale consente di poter partecipare a un'esperienza condivisa stando nello stesso ambiente, potendo vedere le manipolazioni effettuate dagli altri utenti, che possono accedere tramite un dispositivo Android o un dipositivo HoloLens 2.

### Istruzioni per l'esecuzione ###

 - Una volta importato il repository, aggiungere il progetto ad Unity Hub e procedere alla sua apertura.
 - Una volta che il porgetto è stato aperto correttamente, spostarsi nella scena principale, andando su **Assets/Scenes/** aprendo la scena *MainScene.unity*.
 - Aprire le impostazioni di progetto cliccando su **File** -> **Build Settings** a questo punto cambiare:

   - La piattaforma in *Android* cliccando sul pulsante **Switch Platform** dopo la selezione,
   - In Run Device selezionate il vostro dispositivo, il quale sarà visibile una volta collegato al PC.

- Dalle impostazioni di Build cliccare sul pulsante **Player Settings**, andare sulla voce **Player** e verificare che:

  - La piattaforma scelta sia *Android*, quindi che l'icona selezionata sia quella di Android,
  - Aprire il menu **Other Settings** e verificare che nelle **Graphics APIs** non sia presente l'API *Vulkan*
  - Sempre in **Other Settings** verificare che il Minimum API Level sia impostato su *24*

### Istruzioni per il deployment ###

- Andando in **File** -> **Build Settings** si può procedere alla build del porgetto cliccando sul pulsante **Build and Run**, creando una nuova cartella **Build** all'interno del progetto dove poter salvare l'apk, ricordando che questa cartella non verra tracciata da git,
- Al termine della build, se tutto è avvenuto correttamente l'applicazione dovrebbe aprirsi automaticamente sul vostro dispositivo
- Si potrà inoltre trovare l'applicazione sul dispositivo con il nome *AndroidAppVuforia*, che può essere modificato spostandosi in Unity andando su **Build Settings** -> **Player Settings** alla voce **Player** selezionando il menu **Icon**, cercando la voce **Short Name**.

### Problema "Black screen" ###

- Se all'avvio dell'applicazione la fotocamera non si apre corretamente ma vi viene mostrato uno schermo nero richiudete l'applicazione sul dispositivo Android e riapritela aspettando qualche secondo prima di toccare l'interfaccia grafica.

### Note da Ricordare ###

L'indirizzo del server viene immesso all'avvio dell'applicazione dall'utente, tramite l'apposita GUI che li viene presentata, se si sta utilizzando l'indirizzo IP privato del server allora, **client e server per poter comunicare devono essere connessi alla stessa rete**, verificare questo tramite le impostazioni di rete.

Una volta che la connessione è andata a buon fine si potrà visualizzare l'ologramma e procedere alla sua manipolazione.
