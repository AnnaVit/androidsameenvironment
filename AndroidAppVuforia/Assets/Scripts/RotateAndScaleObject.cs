﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotateAndScaleObject : MonoBehaviour
{
    [SerializeField]
    private Slider scaleSlider;
    
    [SerializeField]
    private Slider rotationSliderX;

    [SerializeField]
    private Slider rotationSliderY;

    [SerializeField]
    private Slider rotationSliderZ;

    [SerializeField]
    private GameObject hologram;

    [SerializeField]
    private UpdateObject updater;

    [SerializeField]
    private Button button;

    [SerializeField]
    private GameObject controls;

    private float currentRotX;
    private float currentRotY;
    private float currentRotZ;
    private float currentScale;

    private bool hasControl;

    public float CurrentRotX { get => currentRotX; set => currentRotX = value; }
    public float CurrentRotY { get => currentRotY; set => currentRotY = value; }
    public float CurrentRotZ { get => currentRotZ; set => currentRotZ = value; }


    // Start is called before the first frame update
    void Start()
    {
        currentRotX = hologram.transform.rotation.x;
        currentRotY = hologram.transform.rotation.y;
        currentRotZ = hologram.transform.rotation.z;

        hasControl = false;

        AddListenerSlider();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void UpdateRotation()
    {
        hologram.transform.localEulerAngles = new Vector3(currentRotX, currentRotY, currentRotZ);
        updater.SendEulerAngle();
    }

    private void RemoveListenerSliders()
    {
        rotationSliderX.onValueChanged.RemoveListener(UpdateRotationX);
        rotationSliderY.onValueChanged.RemoveListener(UpdateRotationY);
        rotationSliderZ.onValueChanged.RemoveListener(UpdateRotationZ);
        scaleSlider.onValueChanged.RemoveListener(UpdateScale);
    }

    private void SetValueSlider(Slider slider, float value)
    {
        slider.value = value;
    }

    private void AddListenerSlider()
    {
        rotationSliderX.onValueChanged.AddListener(UpdateRotationX);
        rotationSliderY.onValueChanged.AddListener(UpdateRotationY);
        rotationSliderZ.onValueChanged.AddListener(UpdateRotationZ);
        scaleSlider.onValueChanged.AddListener(UpdateScale);
    }

    public void UpdateScale(float value)
    {
        hologram.transform.localScale = new Vector3(value, value, value);
        updater.SendScale();
    }

    public void UpdateRotationX(float value)
    {
        currentRotX = value;
        this.UpdateRotation();
    }
    public void UpdateRotationY(float value)
    {
       currentRotY = value; 
        this.UpdateRotation();
    }

    public void UpdateRotationZ(float value)
    {
        currentRotZ = value;
        this.UpdateRotation();
    }

    private void UpdateSliderPointer()
    {
        RemoveListenerSliders();

        SetValueSlider(rotationSliderX, currentRotX);
        SetValueSlider(rotationSliderY, currentRotY);
        SetValueSlider(rotationSliderZ, currentRotZ);

        SetValueSlider(scaleSlider, hologram.transform.localScale.x);

        AddListenerSlider();
    }

    public void ManageControl() 
    {
        if (!hasControl)
        {
            UpdateSliderPointer();

            Debug.Log("current rot x " + currentRotX );

            controls.SetActive(true);
            button.GetComponentInChildren<Text>().text = "Relese Control";
            hasControl = true;
            updater.RequestControl();
        }
        else 
        {
            controls.SetActive(false);
            button.GetComponentInChildren<Text>().text = "Request Control";
            hasControl = false;
            updater.ReleseControl();
        }
    }

}
