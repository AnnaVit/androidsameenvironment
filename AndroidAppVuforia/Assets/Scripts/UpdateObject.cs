﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum Manipulation { Mooving = 1, Rotating = 2, Scaling = 3, None = 4 }

public class UpdateObject : MonoBehaviour
{
    [SerializeField]
    GameObject hologram;

    [SerializeField]
    GameObject debugLog;

    [SerializeField]
    RotateAndScaleObject rotAndScale;

    private Color myColor;
   
   
    public Color MyColor { get => myColor; set => myColor = value; }

    public void ShowHologram() 
    {
        hologram.SetActive(true);
        debugLog.SetActive(false);
    }

    public void RequestControl()
    {
        DataSender.RequestControl(JsonUtility.ToJson(myColor));
    }

    public void ReleseControl()
    {
        DataSender.ReleseControl();
    }

    private void Start()
    {
        DataReceiver.UpdateObject = this;
    }

    public void SendRotation()
    {
        DataSender.SendRotation(JsonUtility.ToJson(hologram.transform.rotation));
        //debugLog.GetComponent<Text>().text = "send rotation";
    }

    public void SendScale()
    {
        DataSender.SendScale(JsonUtility.ToJson(hologram.transform.localScale));
        //debugLog.GetComponent<Text>().text = "send scale";
    }

    public void SendPosition()
    {
        DataSender.SendPosition(JsonUtility.ToJson(hologram.transform.position));
        //debugLog.GetComponent<Text>().text = "send position";
    }

    public void SendEulerAngle()
    {
        DataSender.SendEulerAngle(JsonUtility.ToJson(hologram.transform.eulerAngles));
    }

    public void UpdatePosition(Vector3 position)
    {
       hologram.transform.position = position;
    }

    public void UpdateRotation(Quaternion rotation)
    {
        hologram.transform.rotation = rotation;
        rotAndScale.CurrentRotX = rotation.eulerAngles.x;
        rotAndScale.CurrentRotY = rotation.eulerAngles.y;
        rotAndScale.CurrentRotZ = rotation.eulerAngles.z;
    }

    public void UpdateScale(Vector3 scale)
    {
        hologram.transform.localScale = scale;
    }

    public void UpdateEulerAngle(Vector3 eulerAngle)
    {
        hologram.transform.localEulerAngles = eulerAngle;
        rotAndScale.CurrentRotX = eulerAngle.x;
        rotAndScale.CurrentRotY = eulerAngle.y;
        rotAndScale.CurrentRotZ = eulerAngle.z;
    }
}
