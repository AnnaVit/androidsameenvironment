﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandleControls : MonoBehaviour
{
    [SerializeField]
    GameObject hologram;

    [SerializeField]
    Button buttonControl;

    private void Start()
    {
        DataReceiver.HandleControls = this;
    }


    public void RemoveControlsComponent(Color colorMaster)
    {
        hologram.GetComponent<DimBoxes.BoundBox>().enabled = true;
        hologram.GetComponent<DimBoxes.BoundBox>().lineColor = colorMaster;
        hologram.GetComponent<DimBoxes.BoundBox>().init();

        buttonControl.interactable = false;
       
    }

    public void RestoreControlsComponent()
    {
        hologram.GetComponent<DimBoxes.BoundBox>().enabled = false;

        buttonControl.interactable = true;
    }
}
