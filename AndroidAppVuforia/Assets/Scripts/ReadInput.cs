﻿using Assets.Scripts;
using DimBoxes;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ReadInput : MonoBehaviour
{
    private string input;

    [SerializeField]
    private GameObject textInputField;
    
    [SerializeField]
    GameObject debugLog;

    [SerializeField]
    private GameObject inputField;
    
    [SerializeField]
    private GameObject buttonEnter;

    [SerializeField]
    private GameObject buttonControls;

    [SerializeField]
    private NetworkManager networkManager;


    public void Readtext()
    {
        this.input = textInputField.GetComponent<Text>().text;
        IPAddress address;
        if (IPAddress.TryParse(input, out address))
        {
            inputField.SetActive(false);
            buttonEnter.SetActive(false);

            debugLog.GetComponent<TextMeshProUGUI>().text = "Trying to connect to the server...";
            buttonControls.SetActive(true);

            networkManager.ConnectClient(this.input);
        }
        else {
            debugLog.GetComponent<TextMeshProUGUI>().text = "This isn't a valid IP address";
        }
    }
}
