﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using Assets.Externals;

namespace Assets.Scripts
{
    static class ClientTCP
    {
        private static TcpClient clientSocket;
        private static NetworkStream stream;
        private static byte[] recBuffer;

        public static string ServerAddress { get; internal set; }

        public static void InitializingNetworking()
        {
            clientSocket = new TcpClient();
            clientSocket.ReceiveBufferSize = 4096;
            clientSocket.SendBufferSize = 4096;
            recBuffer = new byte[4096 * 2]; //perchè riceviamo e inviamo i dati nello stesso momento
            clientSocket.BeginConnect(ServerAddress, 8080, new AsyncCallback(ClientConnectCallback), clientSocket);
        }

        private static void ClientConnectCallback(IAsyncResult result)
        {
            clientSocket.EndConnect(result);

            if (clientSocket.Connected == false)
            {
                return;
            }
            else 
            {
                clientSocket.NoDelay = true;
                stream = clientSocket.GetStream();
                stream.BeginRead(recBuffer, 0, 4096 * 2, ReciveCallBack, null);

            }
        }

        private static void ReciveCallBack(IAsyncResult result)
        {
            try
            {
                int lenght = stream.EndRead(result);
                if (lenght <= 0)
                {
                    return;
                }

                byte[] newBytes = new byte[lenght];
                Array.Copy(recBuffer, newBytes, lenght);
                UnityThread.executeInFixedUpdate(() =>
                {
                    ClientHandleData.HandleData(newBytes);
                });

                stream.BeginRead(recBuffer, 0, 4096 * 2, ReciveCallBack, null);
            }
            catch (Exception)
            {
                return;
            }
        }

        public static void SendData(byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteInt(data.GetUpperBound(0) - data.GetLowerBound(0) + 1); //no data.lenght perchè se ci sono più array in data potrebbe dare degli errori
            buffer.WriteBytes(data);
            stream.BeginWrite(buffer.ToArray(), 0, buffer.ToArray().Length, null, null);
            buffer.Dispose();
        }

        public static void Disconnect()
        {
            clientSocket.Close();
        }
    }
}
